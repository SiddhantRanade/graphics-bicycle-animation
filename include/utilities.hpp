#ifndef _UTILITIES_HPP_
#define _UTILITIES_HPP_

namespace utilities {
	void LoadTexture( const char * filename, GLuint &texture);
	void LoadDummyTexture(GLuint &texture);
	void beginTexture2D(const size_t i);
	void endTexture2D();
	float mySin(const float angle);
	float myCos(const float angle);

	enum Material {SPECULAR, DIFFUSE};
	const float spec_s[] = {1,1,1,1};
	const float diff_s[] = {0.1,0.1,0.1,1};
	void setMaterialType(Material m);

	enum Light {HEADLIGHT, LAMP};
	const float headlight_c[] = {0.7, 0.7, 0.5, 1.0};
	const float lamp_c[] = {0.5, 0.6, 0.7, 1.0};
	const float off_c[] = {0, 0, 0, 1.0};
	const float ambient_c[] = {0.1, 0.1, 0.1, 1.0};
	void toggleLight(Light l);
	void solve(std::vector<double> &a, std::vector<double> &b, std::vector<double> &c, std::vector<double> &d, int n);
}

#endif