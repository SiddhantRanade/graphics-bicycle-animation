#ifndef _BICYCLE_FUNCS_HPP_
#define _BICYCLE_FUNCS_HPP_

void unitTriangularPrism (bool solid = true);
void cylinder (const float radius, const float height);
void drawCylinder (const float x1, const float y1, const float z1, const float x2, const float y2, const float z2, const float radius);
void drawCylinder (const float* v1, const float* v2, const float radius);
void wheel (const int num_spokes = 12);
void seat();
void front();
void pedal();
void frame();
void drawPedals();

class Bicycle {
private:
	// GLfloat scale;
	GLfloat handleAngle, pedalAngle, frontWheelAngle, rearWheelAngle;
	GLfloat x, y, z;
	GLfloat theta, phi;		// theta is angle made with the vertical axis ---> to be used for banking
							// phi is the angle of rotation on the ground ---> to be used for turning
	static int* displayListIndexArray;

	void PedalF(const float delta = 1.);
	void PedalB(const float delta = 1.);
	void RearWheelF(const float delta = 1.);
	void RearWheelB(const float delta = 1.);
	void FrontWheelF(const float delta = 1.);
	void FrontWheelB(const float delta = 1.);
	void HandleL(const float delta = 1.);
	void HandleR(const float delta = 1.);

	void Theta(const float delta);
	void Phi(const float delta);

public:
	Bicycle(float _x = 0, float _y = 0, float _z = 0, float _theta = 0, float _phi = 0)
		: x(_x), y(_y), z(_z), theta(_theta), phi(_phi), 
		handleAngle(0), pedalAngle(0), frontWheelAngle(0), rearWheelAngle(0) {}
	void draw();

	void forward();
	void left();
	void right();

	GLfloat getX() const {return x;}
	GLfloat getY() const {return y;}
	GLfloat getZ() const {return z;}
	GLfloat getHandleAngle() const {return handleAngle;}
	GLfloat getPedalAngle() const {return pedalAngle;}
	GLfloat getFrontWheelAngle() const {return frontWheelAngle;}
	GLfloat getRearWheelAngle() const {return rearWheelAngle;}
	GLfloat getPhi() const {return phi;}

	void set(GLfloat x_, GLfloat y_, GLfloat z_, GLfloat phi_, GLfloat handleAngle_) {
		x = x_; y = y_; z = z_; phi = phi_; handleAngle = handleAngle_;
	}
	void turn(GLfloat dP, GLfloat dR, GLfloat dF) {
		pedalAngle += dP; rearWheelAngle += dR; frontWheelAngle += dF;
	}
};

namespace BICYCLE{
	// Coordinate data for bicycle frame
	extern const float vertices[15][3];
	extern const float vertices1[10][3];

	extern const float Length;
	extern const float wheelSize;
	extern const float gearRatio;
	extern const float radius_ratio;

	extern const float step;

	extern const float cosCasterAngle;

	extern const float degToRad;
	extern const float radToDeg;

	extern const float dRAngle;
	extern const float dr;
	extern const float dPhiHalfConstFactor;
}

#endif