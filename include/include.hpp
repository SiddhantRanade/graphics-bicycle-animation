#ifndef _INCLUDE_HPP_
#define _INCLUDE_HPP_

#include <cmath>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <queue>
#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <stdexcept>

#ifdef __APPLE__
#include <OpenGL/gl3.h>
#endif
#ifndef __APPLE__
#include <GL/glew.h>
#endif

#ifdef __APPLE__
#include <GLUT/glut.h>  //Include for MAC
#endif

#ifndef __APPLE__
#include <GL/glut.h>
#endif

#include "GLShaderUtils.h"
#include "utilities.hpp"
#include "bicycle_funcs.hpp"
#include "room.hpp"

#endif