#include "include.hpp"

extern int currentDisplayList;

Room::Room () {
}

void Room::draw () const {
	glColor4f(1.0, 1.0, 0.75, 1.0);
	glPushMatrix();
	
	utilities::beginTexture2D(1);
	glBegin(GL_QUADS);
	/* Floor */
	glNormal3f(0.0, 1.0, 0.0);
	glTexCoord2f(0.0, 0.0);  glVertex3f(roomSize, -0.05, roomSize);
	glTexCoord2f(0.0, 1.0);  glVertex3f(roomSize, -0.05, -roomSize);
	glTexCoord2f(1.0, 1.0);  glVertex3f(-roomSize, -0.05, -roomSize);
	glTexCoord2f(1.0, 0.0);  glVertex3f(-roomSize, -0.05, roomSize);
	glEnd();
	utilities::endTexture2D();
	// glColor4fv(currentColor);

	/* Ceiling */
	glBegin(GL_QUADS);
	glNormal3f(0.0, -1.0, 0.0);
	glVertex3f(-roomSize, roomHeight, -roomSize);
	glVertex3f(roomSize, roomHeight, -roomSize);
	glVertex3f(roomSize, roomHeight, roomSize);
	glVertex3f(-roomSize, roomHeight, roomSize);
	
	/* Walls */
	glNormal3f(0.0, 0.0, -1.0);
	glVertex3f(roomSize, -0.05, roomSize);
	glVertex3f(-roomSize, -0.05, roomSize);
	glVertex3f(-roomSize, roomHeight, roomSize);
	glVertex3f(roomSize, roomHeight, roomSize);
	glEnd();

	/* Light fixture */
	glPushMatrix();
	// glColor4f(0.0, 0.0, 0.0, 1.0);
	glTranslatef(0.0, roomHeight-1.5, 0.0);

	// Rod to attach to ceiling
	glPushMatrix();
	glTranslatef(0.0, 1.5, 0.0);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	cylinder(0.05, 0.95);
	glPopMatrix();
	
	// 3 horizontal rods
	glPushMatrix();
	glTranslatef(0.0, 0.725, 0.0);
	glRotatef(90, 0.0, 1.0, 0.0);
	cylinder(0.025, 0.59);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(0.0, 0.725, 0.0);
	glRotatef(240, 0.0, 1.0, 0.0);
	glRotatef(90, 0.0, 1.0, 0.0);
	cylinder(0.025, 0.59);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(0.0, 0.725, 0.0);
	glRotatef(120, 0.0, 1.0, 0.0);
	glRotatef(90, 0.0, 1.0, 0.0);
	cylinder(0.025, 0.59);
	glPopMatrix();

	// Center sphere
	glPushMatrix();
	glTranslatef(0.0, 0.725, 0.0);
	// glPushMatrix();
	glutSolidSphere(0.1, 12, 12);
	GLfloat light_position1[] = { 0.0, 0.0, 0.0, 1.0 };
	glLightfv(GL_LIGHT2, GL_POSITION, light_position1);
	GLfloat light_direction[] = { 0.0, -1.0, 0.0 };
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light_direction);
	// glPopMatrix();
	glPopMatrix();

	// Cylindrical part of lamp
	glColor4f(0.0, 0.0, 0.0, 0.75);
	glPushMatrix();
	glTranslatef(0.0, 1.0, 0.0);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	cylinder(.60, 0.45);
	glPopMatrix();
	glPopMatrix();

	/* Painting */
	// glGetFloatv(GL_CURRENT_COLOR,currentColor);
	glColor4f(1.0, 1.0, 0.75, 1.0);

	utilities::beginTexture2D(3);
	glBegin(GL_QUADS);
	glNormal3f(0.0, 0.0, 1.0);
	// glColor4f(1.0, 0.75, 1.0, 1.0);
	glTexCoord2f(0.0, 0.0); glVertex3f(-1.2, 1.5, -roomSize + 0.05);
	glTexCoord2f(1.0, 0.0); glVertex3f(1.2, 1.5, -roomSize + 0.05);
	glTexCoord2f(1.0, 1.0); glVertex3f(1.2, 3.5, -roomSize + 0.05);
	glTexCoord2f(0.0, 1.0); glVertex3f(-1.2, 3.5, -roomSize + 0.05);
	glEnd();
	utilities::endTexture2D();
	// glColor4fv(currentColor);

	// glColor4f(1.0, 1.0, 0.75, 1.0);
	glBegin(GL_QUADS);
	glNormal3f(0.0, 0.0, 1.0);
	glVertex3f(-roomSize, -0.05, -roomSize);
	glVertex3f(roomSize, -0.05, -roomSize);
	glVertex3f(roomSize, roomHeight, -roomSize);
	glVertex3f(-roomSize, roomHeight, -roomSize);

	glNormal3f(-1.0, 0.0, 0.0);
	// glColor4f(0.75, 1.0, 1.0, 1.0);
	glVertex3f(roomSize, -0.05, roomSize);
	glVertex3f(roomSize, roomHeight, roomSize);
	glVertex3f(roomSize, roomHeight, -roomSize);
	glVertex3f(roomSize, -0.05, -roomSize);

	glNormal3f(1.0, 0.0, 1.0);
	// glColor4f(1.0, 1.0, 1.0, 1.0);
	glVertex3f(-roomSize, roomHeight, roomSize);
	glVertex3f(-roomSize, -0.05, roomSize);
	glVertex3f(-roomSize, -0.05, -roomSize);
	glVertex3f(-roomSize, roomHeight, -roomSize);
	glEnd();
	
	glPopMatrix();
}