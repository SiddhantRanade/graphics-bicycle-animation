#include "include.hpp"

#define angleDiscretization 100		// how many parts does a degree get divided into?

extern GLuint *textures;
extern GLuint useTexIndex;
extern GLuint pps_program;
extern int light_num[2];

namespace {
	const double degToRad = M_PI/180.;
}
void utilities::LoadTexture( const char * filename, GLuint &texture) {
	unsigned char header[54];// 54 Byte header of BMP
	int pos;
	unsigned int w,h;
	unsigned int size; //w*h*3
	unsigned char * data; // Data in RGB FORMAT
	FILE * file;

	file = fopen( filename, "rb" );
	if ( file == NULL ) return;  // if file is empty
	if (fread(header,1,54,file)!=54){
		 printf("Incorrect BMP file\n");
		 return;
	}

	// Read  MetaData
	pos = *(int*)&(header[0x0A]);
	size = *(int*)&(header[0x22]);
	w = *(int*)&(header[0x12]);
	h = *(int*)&(header[0x16]);

	std::cout << filename << ": " << w <<" x "<< h << std::endl;

	//Just in case metadata is missing
	if(size == 0)
	size = w*h*3;
	if(pos == 0)
	pos = 54;

	data = new unsigned char [size];

	fread( data, size, 1, file ); // read the file
	fclose( file );
	//////////////////////////

	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	// glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE_EXT );
	// glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE_EXT );

	glBindTexture( GL_TEXTURE_2D, texture );
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	gluBuild2DMipmaps( GL_TEXTURE_2D, GL_RGBA, w, h, GL_BGR, GL_UNSIGNED_BYTE, data );

	free( data );
}

void utilities::LoadDummyTexture(GLuint &texture) {
	unsigned char* data = new unsigned char [3];
	data[0] = 255;
	data[1] = 255;
	data[2] = 255;
	//////////////////////////

	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	// glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE_EXT );
	// glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE_EXT );

	glBindTexture( GL_TEXTURE_2D, texture );
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	free( data );
}

void utilities::beginTexture2D(const size_t i) {
	// glActiveTexture(textures[i]);
	glBindTexture(GL_TEXTURE_2D, textures[i]);
	glEnable(GL_TEXTURE_2D);
	glVertexAttrib1f(useTexIndex, 1.0);
}

void utilities::endTexture2D() {
	// glActiveTexture(textures[i]);
	glBindTexture(GL_TEXTURE_2D, 0);
	glEnable(GL_TEXTURE_2D);
	glVertexAttrib1f(useTexIndex, 0.0);
}

float utilities::mySin(float angle) {
	static float* sinTable = NULL;
	if(sinTable == NULL) {
		sinTable = new float[360 * angleDiscretization];
		for (int i = 0; i < 360 * angleDiscretization; ++i) {
			sinTable[i] = std::sin((double)i /10. * degToRad);
		}
	}
	angle = std::fmod(angle, 360);
	if(angle < 0) angle += 360.;
	return sinTable[(int) (angle * 10.)];
}

float utilities::myCos(float angle) {
	static float* cosTable = NULL;
	if(cosTable == NULL) {
		cosTable = new float[360 * angleDiscretization];
		for (int i = 0; i < 360 * angleDiscretization; ++i) {
			cosTable[i] = std::cos((double)i /10. * degToRad);
		}
	}
	angle = std::fmod(angle, 360);
	if(angle < 0) angle += 360.;
	return cosTable[(int) (angle * 10.)];
}

void utilities::setMaterialType(utilities::Material m) {
	switch(m) {
		case SPECULAR:
			glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, utilities::spec_s);
			break;
		case DIFFUSE:
			glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, utilities::diff_s);
			break;
	}
}

void utilities::toggleLight(utilities::Light l){
	switch(l){
		case HEADLIGHT: {
			GLboolean b;
			glGetBooleanv(GL_LIGHT1, &b);
			if(b == GL_TRUE) {
				glLightfv(GL_LIGHT1, GL_DIFFUSE, utilities::off_c);
				glLightfv(GL_LIGHT1, GL_SPECULAR, utilities::off_c);
				glDisable(GL_LIGHT1);
			}
			else {
				glLightfv(GL_LIGHT1, GL_DIFFUSE, utilities::headlight_c);
				glLightfv(GL_LIGHT1, GL_SPECULAR, utilities::headlight_c);
				glEnable(GL_LIGHT1);
			}
			break;
		}
		case LAMP: {
			GLboolean b;
			glGetBooleanv(GL_LIGHT2, &b);
			if(b == GL_TRUE) {
				glLightfv(GL_LIGHT2, GL_DIFFUSE, utilities::off_c);
				glLightfv(GL_LIGHT2, GL_SPECULAR, utilities::off_c);
				glDisable(GL_LIGHT2);
			}
			else {
				glLightfv(GL_LIGHT2, GL_DIFFUSE, utilities::lamp_c);
				glLightfv(GL_LIGHT2, GL_SPECULAR, utilities::lamp_c);
				glEnable(GL_LIGHT2);
			}
			break;
		}
	}
}

void utilities::solve(std::vector<double> &a, std::vector<double> &b, std::vector<double> &c, std::vector<double> &d, int n) {
    /*
    // n is the number of unknowns

    |b0 c0 0 ||x0| |d0|
    |a1 b1 c1||x1|=|d1|
    |0  a2 b2||x2| |d2|

    1st iteration: b0x0 + c0x1 = d0 -> x0 + (c0/b0)x1 = d0/b0 ->

        x0 + g0x1 = r0               where g0 = c0/b0        , r0 = d0/b0

    2nd iteration:     | a1x0 + b1x1   + c1x2 = d1
        from 1st it.: -| a1x0 + a1g0x1        = a1r0
                    -----------------------------
                          (b1 - a1g0)x1 + c1x2 = d1 - a1r0

        x1 + g1x2 = r1               where g1=c1/(b1 - a1g0) , r1 = (d1 - a1r0)/(b1 - a1g0)

    3rd iteration:      | a2x1 + b2x2   = d2
        from 2st it. : -| a2x1 + a2g1x2 = a2r2
                       -----------------------
                       (b2 - a2g1)x2 = d2 - a2r2
        x2 = r2                      where                     r2 = (d2 - a2r2)/(b2 - a2g1)
    Finally we have a triangular matrix:
    |1  g0 0 ||x0| |r0|
    |0  1  g1||x1|=|r1|
    |0  0  1 ||x2| |r2|

    Condition: ||bi|| > ||ai|| + ||ci||

    in this version the c matrix reused instead of g
    and             the d matrix reused instead of r and x matrices to report results
    Written by Keivan Moradi, 2014
    */
    n--; // since we start from x0 (not x1)
    c[0] /= b[0];
    d[0] /= b[0];

    for (int i = 1; i < n; i++) {
        c[i] /= b[i] - a[i]*c[i-1];
        d[i] = (d[i] - a[i]*d[i-1]) / (b[i] - a[i]*c[i-1]);
    }

    d[n] = (d[n] - a[n]*d[n-1]) / (b[n] - a[n]*c[n-1]);

    for (int i = n; i-- > 0;) {
        d[i] -= c[i]*d[i+1];
    }
}