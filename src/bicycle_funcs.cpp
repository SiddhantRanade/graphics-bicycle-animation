#include "include.hpp"

extern int currentDisplayList;

namespace BICYCLE{
	// Coordinate data for bicycle frame
	const float vertices[15][3] = {
		{-1.56186613,	0.00000000,		0.00000000},
		{-0.67748479,	-0.21906694,		0.00000000},
		{-0.76673428,	-0.68154158,		0.00000000},
		{-1.59432049,	-0.08113590,		0.00000000},
		{-1.65111562,	0.22312373,		0.00000000},
		{-1.50912779,	0.18255578,		0.00000000},
		{-1.69979716,	0.36511156,		0.00000000},
		{-0.64908723,	-0.34888438,		0.00000000},
		{0.00000000,	-0.78000000,		-0.08113590},
		{0.00000000,	-0.78000000,		0.08113590},
		{-0.59634889,	0.10141988,		0.00000000},			// C
		{-0.67748479,	-0.21906694,		-0.05000000},
		{-0.67748479,	-0.21906694,		0.05000000},
		{-0.76673428,	-0.68154158,		-0.05000000},
		{-0.76673428,	-0.68154158,		0.05000000}
	};

	const float vertices1[10][3] = {{0.05223923,	0.15107020,	0.00000000},
		{-0.07261683,	-0.21,	0.00000000},
		{-0.13843014,	0.31739880,	0.00000000},
		{-0.10,	0.31739880,	-0.5},
		{-0.10,	0.31739880,	0.5},
		{-0.07261683,	-0.21,	-0.08113590},
		{-0.07261683,	-0.21,	0.08113590},
		{-0.26971964,   -0.78,   -0.08113590},
		{-0.26971964,   -0.78,   0.08113590},
		{0.09280718,	0.13484302,	0.00000000}
	};

	const float Length = vertices[8][0] - vertices[0][0] - vertices1[7][0];
	const float wheelSize = 0.5;
	const float gearRatio = 2.;
	const float radius_ratio = 0.1;

	const float step = 2.;

	const float cosCasterAngle = (vertices1[0][1] - vertices1[1][1]) / sqrt(pow((vertices1[0][0] - vertices1[1][0]),2.) + pow((vertices1[0][1] - vertices1[1][1]),2.));

	const float degToRad = M_PI/180.;
	const float radToDeg = 180./M_PI;

	const float dRAngle = step * gearRatio;
	const float dr = degToRad * step * gearRatio * wheelSize * (1 + radius_ratio);
	const float dPhiHalfConstFactor = step * gearRatio * wheelSize * cosCasterAngle * (1 + radius_ratio) / Length / 2;
}

using namespace BICYCLE;

int* Bicycle::displayListIndexArray = NULL;

void doughnut(GLfloat r, GLfloat R, GLint nsides, GLint rings) {
// From https://cgit.freedesktop.org/mesa/glut/tree/src/glut/glx/glut_shapes.c#n178 -- accessed 09/10/2016
	int i, j;
	GLfloat theta, phi, theta1;
	GLfloat cosTheta, sinTheta;
	GLfloat cosTheta1, sinTheta1;
	GLfloat ringDelta, sideDelta;

	ringDelta = 360. / rings;
	sideDelta = 360. / nsides;

	theta = 0.0;
	cosTheta = 1.0;
	sinTheta = 0.0;
	for (i = rings - 1; i >= 0; i--) {
		theta1 = theta + ringDelta;
		cosTheta1 = utilities::myCos(theta1);
		sinTheta1 = utilities::mySin(theta1);
		glBegin(GL_QUAD_STRIP);
		phi = 0.0;
		for (j = nsides; j >= 0; j--) {
			GLfloat cosPhi, sinPhi, dist;

			phi += sideDelta;
			cosPhi = utilities::myCos(phi);
			sinPhi = utilities::mySin(phi);
			dist = R + r * cosPhi;

			glNormal3f(cosTheta1 * cosPhi, -sinTheta1 * cosPhi, sinPhi);
			glVertex3f(cosTheta1 * dist, -sinTheta1 * dist, r * sinPhi);
			glNormal3f(cosTheta * cosPhi, -sinTheta * cosPhi, sinPhi);
			glVertex3f(cosTheta * dist, -sinTheta * dist,  r * sinPhi);
		}
		glEnd();
		theta = theta1;
		cosTheta = cosTheta1;
		sinTheta = sinTheta1;
	}
}

void unitTriangularPrism (bool solid/* = true*/) {
// Adapted from http://stackoverflow.com/questions/3898450/create-a-right-angled-triangular-prism-in-opengl -- accessed 30/09/2016
	// back endcap
	glBegin(solid ? GL_TRIANGLES : GL_LINES);
	glNormal3f(0.0, 0.0, -1.0);
	glVertex3f(1.0, 0.0, 0.0);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(0.0, 1.0, 0.0);
	glEnd();

	// front endcap
	glBegin(solid ? GL_TRIANGLES : GL_LINES);
	glNormal3f(0.0, 0.0, 1.0);
	glVertex3f(0.0, 0.0, 1.0);
	glVertex3f(1.0, 0.0, 1.0);
	glVertex3f(0.0, 1.0, 1.0);
	glEnd();

	// bottom
	glBegin(solid ? GL_QUADS : GL_LINES);
	glNormal3f(0.0, -1.0, 0.0);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(1.0, 0.0, 0.0);
	glVertex3f(1.0, 0.0, 1.0);
	glVertex3f(0.0, 0.0, 1.0);
	glEnd();

	// back
	glBegin(solid ? GL_QUADS : GL_LINES);
	glNormal3f(-1.0, 0.0, 0.0);
	glVertex3f(0.0, 0.0, 1.0);
	glVertex3f(0.0, 1.0, 1.0);
	glVertex3f(0.0, 1.0, 0.0);
	glVertex3f(0.0, 0.0, 0.0);
	glEnd();

	// top
	glBegin(solid ? GL_QUADS : GL_LINES);
	glNormal3f(0.70710678118, 0.70710678118, 0.0);
	glVertex3f(0.0, 1.0, 1.0);
	glVertex3f(1.0, 0.0, 1.0);
	glVertex3f(1.0, 0.0, 0.0);
	glVertex3f(0.0, 1.0, 0.0);
	glEnd();
}

void cylinder (const float radius, const float height) {
	glPushMatrix();
	float r = radius;
	float h = height;
	GLUquadricObj *quadratic;
	quadratic = gluNewQuadric();
	gluQuadricDrawStyle( quadratic, GLU_FILL);  
	gluQuadricNormals( quadratic, GLU_SMOOTH);  
	gluQuadricTexture( quadratic, GL_TRUE );   

	gluCylinder(quadratic, (GLdouble)r, (GLdouble)r, (GLdouble)h, 12, 5);
	glPopMatrix();
}

void drawCylinder (const float x1, const float y1, const float z1, const float x2, const float y2, const float z2, const float radius) {
	float dx = x2 - x1, dy = y2 - y1, dz = z2 - z1;
	float h = std::sqrt(dx*dx + dy*dy + dz*dz);
	float angle = std::acos(dz/h)*180.0/M_PI;
	glPushMatrix();
	glTranslatef(x1, y1, z1);
	glRotatef(angle, -dy, dx, 0);
	cylinder(radius, h);
	glPopMatrix();
}

void drawCylinder (const float* v1, const float* v2, const float radius) {
	drawCylinder(v1[0], v1[1], v1[2], v2[0], v2[1], v2[2], radius);
}

void wheel(const int num_spokes/*= 12*/) {
	float r_axle = radius_ratio/2;
	float h_axle = radius_ratio*4;
	glPushMatrix();
	glRotatef(90,1.0,0.0,0.0);
	float dtheta = 360./num_spokes;

	glColor3f(0.5, 0.5, 0.5);
	for (int i = 0; i < num_spokes; ++i) {
		glTranslatef(r_axle, 0.0, 0.0);
		cylinder(radius_ratio/10, 1 - radius_ratio);
		glTranslatef(-r_axle, 0.0, 0.0);
		glRotatef(dtheta,0.0,1.0,0.0);
	}
	glPopMatrix();

	glColor3f(0.1, 0.1, 0.1);

	doughnut(radius_ratio, 1, 12, 30);
	glTranslatef(0.0, 0.0, -h_axle/2);
	cylinder(r_axle, h_axle);
	glTranslatef(0.0, 0.0, h_axle/2);
}

void seat() {
	glPushMatrix();
	glScalef(1.0, 0.25, 0.4);
	glRotatef(45, 0.0, 1.0, 0.0);
	glRotatef(90, 1.0, 0.0, 0.0);
	unitTriangularPrism();
	glPopMatrix();
}

void front() {
	drawCylinder(vertices1[0], vertices1[1], 0.05);
	drawCylinder(vertices1[2], vertices1[0], 0.05);
	drawCylinder(vertices1[3], vertices1[2], 0.05);
	drawCylinder(vertices1[4], vertices1[2], 0.05);
	drawCylinder(vertices1[5], vertices1[6], 0.03);
	drawCylinder(vertices1[5], vertices1[7], 0.03);
	drawCylinder(vertices1[6], vertices1[8], 0.03);

	// headlight geometry
	glPushMatrix();
	glTranslatef(vertices1[2][0], vertices1[2][1], vertices1[2][2]);
	glTranslatef(-0.15, 0.025, 0.0);
	glRotatef(90.0, 0.0, 1.0, 0.0);
	glPushMatrix();
	glutSolidCone(0.07, 0.15, 30, 2);
	glPushMatrix();
	glTranslatef(0.0, 0.0, 0.07);
	cylinder(0.046, 0.05);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(0.0, 0.0, 0.10);
	glutSolidSphere(0.045, 12, 12);
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(vertices1[0][0], vertices1[0][1], vertices1[0][2]);
	glutSolidSphere(0.05, 12, 12);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(vertices1[5][0], vertices1[5][1], vertices1[5][2]);
	glutSolidSphere(0.03, 12, 12);
	glTranslatef(0, 0, 2 * vertices1[6][2]);
	glutSolidSphere(0.03, 12, 12);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(vertices1[2][0], vertices1[2][1], vertices1[2][2]);
	glutSolidSphere(0.05, 12, 12);

	GLfloat light_position[] = { 0.0, 0.0, 0.0, 1.0 };
	glLightfv(GL_LIGHT1, GL_POSITION, light_position);
	GLfloat headlight_direction[] = { -1.0, 0.0, 0.0 };
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, headlight_direction);
	
	glPopMatrix();

	glPushMatrix();
	glTranslatef(vertices1[7][0], vertices1[7][1], vertices1[7][2]);
	glutSolidSphere(0.03, 12, 12);
	glTranslatef(0, 0, 2 * vertices1[8][2]);
	glutSolidSphere(0.03, 12, 12);
	glPopMatrix();
}

void pedal() {
	glPushMatrix();
	glScalef(1.25, 1.25, 1.25);
	glPushMatrix();
	glScalef(0.25, 0.25, 0.2);
	cylinder(0.1, 0.5);
	glPopMatrix();
	glPushMatrix();
	glScalef(0.0426, 0.29, 0.0426);
	glTranslatef(0.0, -0.4, 0.1);
	glutSolidCube(1);
	glPopMatrix();
	glPushMatrix();
	glScalef(0.092, 0.05, 0.166);
	glTranslatef(0, -4.75, -0.4);
	glPushMatrix();
	glRotatef(90, 0, 0, 1);
	glRotatef(90, 0, 1, 0);
	glutSolidCube(1);
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
}

void frame() {
	// float currentColor[4];
	// glGetFloatv(GL_CURRENT_COLOR,currentColor);
	glColor4f(1,1,1,1);

	utilities::beginTexture2D(0);

	drawCylinder(vertices[0], vertices[1], 0.05); // OA
	drawCylinder(vertices[3], vertices[2], 0.05); // Front lower cylinder
	
	utilities::endTexture2D();

	glColor3f(0.0, 0.7, 1.0);


	drawCylinder(vertices[2], vertices[10], 0.05); // Frame middle vertical cylinder
	drawCylinder(vertices[11], vertices[8], 0.02); // Rear top left
	drawCylinder(vertices[12], vertices[9], 0.02); // Rear top right
	drawCylinder(vertices[13], vertices[8], 0.02); // Rear bottom left
	drawCylinder(vertices[14], vertices[9], 0.02); // Rear bottom right

	glPushMatrix();
	glTranslatef(vertices[2][0] - 0.06, 0.1404, 0.0);
	glScalef(0.45, 0.45, 0.45);
	// glGetFloatv(GL_CURRENT_COLOR,currentColor);
	glColor4f(1,1,1,1);
	utilities::beginTexture2D(2);
	seat();
	utilities::endTexture2D();
	// glColor4fv(currentColor);
	glColor3f(0.0, 0.7, 1.0);

	glPopMatrix();

	glPushMatrix();
	glTranslatef(vertices[1][0], vertices[1][1], vertices[1][2]);
	glutSolidSphere(0.07, 12, 12);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(vertices[2][0], vertices[2][1], vertices[2][2]);
	glutSolidSphere(0.07, 12, 12);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(vertices[8][0], vertices[8][1], vertices[8][2]);
	glutSolidSphere(0.02, 12, 12);
	glTranslatef(0, 0, 2 * vertices[9][2]);
	glutSolidSphere(0.02, 12, 12);
	glPopMatrix();
 }

void drawPedals() {
	glPushMatrix();
	glTranslatef(0, 0, -0.15);
	pedal();
	glTranslatef(0, 0, +2 * 0.15);
	glRotatef(180, 0.0, 1.0, 0.0);
	glRotatef(180, 0.0, 0.0, 1.0);
	pedal();
	glPopMatrix();
}

void Bicycle::PedalF(const float delta/* = 1 */) {
	pedalAngle += delta;
	if(pedalAngle >= 360) pedalAngle -= 360;
}

void Bicycle::PedalB(const float delta/* = 1 */) {
	pedalAngle -= delta;
	if(pedalAngle < 0) pedalAngle += 360;
}

void Bicycle::RearWheelF(const float delta/* = 1 */) {
	rearWheelAngle += delta;
	if(rearWheelAngle >= 360) rearWheelAngle -= 360;
}

void Bicycle::RearWheelB(const float delta/* = 1 */) {
	rearWheelAngle -= delta;
	if(rearWheelAngle < 0) rearWheelAngle += 360;
}

void Bicycle::FrontWheelF(const float delta/* = 1 */) {
	frontWheelAngle += delta;
	if(frontWheelAngle >= 360) frontWheelAngle -= 360;
}

void Bicycle::FrontWheelB(const float delta/* = 1 */) {
	frontWheelAngle -= delta;
	if(frontWheelAngle < 0) frontWheelAngle += 360;
}

void Bicycle::HandleR(const float delta/* = 1 */) {
	handleAngle -= delta;
	if(handleAngle < 0) handleAngle += 360;
}

void Bicycle::HandleL(const float delta/* = 1 */) {
	handleAngle += delta;
	if(handleAngle >= 360) handleAngle -= 360;
}

void Bicycle::Theta(const float delta) {
	theta += delta;
	if(theta < 0) theta += 360;
	if(theta >= 360) theta -= 360;
}

void Bicycle::Phi(const float delta) {
	phi += delta;
	if(phi < 0) phi += 360;
	if(phi >= 360) phi -= 360;
}

void Bicycle::forward() {
	PedalF(step);
	RearWheelF(step * gearRatio);

	float dFAngle = dRAngle / utilities::myCos(handleAngle);
	float dPhiHalf = dPhiHalfConstFactor * utilities::mySin(handleAngle) / utilities::myCos(handleAngle);
	
	Phi(dPhiHalf);
	float c = utilities::myCos(phi);
	float s = utilities::mySin(phi);
	x += - dr * c + vertices[8][0] * 2 * s * dPhiHalf * degToRad;
	z += + dr * s + vertices[8][0] * 2 * c * dPhiHalf * degToRad;
	Phi(dPhiHalf);
	FrontWheelF(dFAngle);
}

void Bicycle::left() {
	HandleL(step);
	// Theta(step);
}

void Bicycle::right() {
	HandleR(step);
	// Theta(-step);
}

void Bicycle::draw() {
	bool firstDraw = false;
	if (displayListIndexArray == NULL) {
		firstDraw = true;
		displayListIndexArray = new int[4];
		std::cerr << "First run, currentDisplayList = " << currentDisplayList << std::endl;
	}

	glPushMatrix();
	glTranslatef(x,y,z);
	glRotatef(phi, 0, 1, 0);
	glRotatef(theta, 1, 0, 0);
	glTranslatef(0, -vertices[8][1] + wheelSize, 0);

	if (firstDraw) {
		glNewList(currentDisplayList, GL_COMPILE);
		glColor3f(0.0, 0.7, 1.0);
		frame();
		glEndList();
		displayListIndexArray[0] = currentDisplayList;
		currentDisplayList++;
	}
	glCallList(displayListIndexArray[0]);

	// Pedals
	glPushMatrix();
	glTranslatef(vertices[2][0], vertices[2][1], vertices[2][2]);
	glRotatef(pedalAngle, 0, 0, 1);

	if (firstDraw) {
		glNewList(currentDisplayList, GL_COMPILE);
		glColor3f(0.3, 0.3, 0.3);
		drawPedals();
		glEndList();
		displayListIndexArray[1] = currentDisplayList;
		currentDisplayList++;
	}
	glCallList(displayListIndexArray[1]);

	glPopMatrix();

	// Rear wheel
	glPushMatrix();
	glTranslatef(vertices[9][0], vertices[9][1], 0);
	glRotatef(rearWheelAngle, 0, 0, 1);

	if (firstDraw) {
		glNewList(currentDisplayList, GL_COMPILE);
		glScalef(wheelSize, wheelSize, wheelSize);
		glColor3f(0.1, 0.1, 0.1);
	
		// GLfloat mat_specular[] = { 0, 0, 0, 1.0 };
		// GLint shininess = 1;
		// glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
		// glMateriali(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
		wheel();
		glEndList();
		displayListIndexArray[2] = currentDisplayList;
		currentDisplayList++;
	}
	glCallList(displayListIndexArray[2]);

	glPopMatrix();
	// Front Fork and wheel
	glPushMatrix();
	glTranslatef(vertices[0][0], vertices[0][1], vertices[0][2]);
	glRotatef(handleAngle, vertices1[0][0] - vertices1[1][0], vertices1[0][1] - vertices1[1][1], vertices1[0][2] - vertices1[1][2]);
	if (firstDraw) {
		glNewList(currentDisplayList, GL_COMPILE);
		glColor3f(0.0, 0.7, 1.0);
		front();
		glTranslatef(vertices1[7][0], vertices1[7][1], 0);
		glEndList();
		displayListIndexArray[3] = currentDisplayList;
		currentDisplayList++;
	}
	glCallList(displayListIndexArray[3]);

	glRotatef(frontWheelAngle, 0, 0, 1);
	glCallList(displayListIndexArray[2]);
	glPopMatrix();

	glPopMatrix();
}