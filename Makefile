CC = c++

LIBS = -std=c++11

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
    LIBS += -lGL -lglut -lGLU -lGLEW
endif
ifeq ($(UNAME_S),Darwin)
    LIBS += -framework OpenGL -framework GLUT
endif

MYINCLUDE = -I include/

SRCS = $(wildcard src/*.cpp)
OBJS = $(addprefix obj/,$(notdir $(SRCS:.cpp=.o)))
MAIN = main.cpp
BIN = bicycle

all	:	$(BIN)

obj/%.o : src/%.cpp
	$(CC) -c $< -o $@ $(LIBS) $(MYINCLUDE)

$(BIN) : $(MAIN) $(OBJS)
	$(CC) $(MAIN) $(OBJS) -o $@ $(LIBS) $(MYINCLUDE)

clean	:
	rm -f $(BIN) $(OBJS)
