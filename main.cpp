#include "include.hpp"

#define nLists 4
int currentDisplayList;

Bicycle *b1;
Room *r1;

GLuint *textures;
std::vector<std::string> fileNames = {"textures/Bicycle.bmp", "textures/Floor.bmp", "textures/leather.bmp", "textures/painting.bmp"};

int currentCamera = 2;

extern const float roomSize;
extern const float roomHeight;

extern const float BICYCLE::Length;

GLuint pps_program;
GLuint texLocation;
GLuint useTexIndex = 1;

std::ofstream keyframeFile;
std::string keyframeFilename = "keyframes.txt";

float t = 0;		// parameter t for interpolation

// const float timeMultiplier = 1./5000;
const float deltat = 1/120.;
// const float deltat = 1/10.;
std::vector<std::vector<double> > state;		// x, y, z, phi
std::vector<int> cameraNum;
std::vector<std::vector<bool> > lightState;		// l1, l2
std::vector<double> xVelocities;
std::vector<double> zVelocities;
double coeffs[8];		// Ax, Bx, Cx, Dx, Ay, By, Cy, Dy

int currentSection = 0;

const float velocity = 5.0;	// speed at start and end
long int lastTime = 0;

bool saveFramesToFile = false;
int frameNum = 0;

static void init (void) {
	glEnable(GL_DEPTH_TEST);

	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_ambient[] = { 0.15, 0.15, 0.15, 1.0 };
	GLint shininess = 100;
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };
	glClearColor (0.7, 0.8, 0.9, 1.0);
	glShadeModel (GL_SMOOTH);

	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);
	
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glLightfv(GL_LIGHT1, GL_AMBIENT, mat_ambient);
	glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 30.0);

	glLightfv(GL_LIGHT1, GL_AMBIENT, mat_ambient);
	glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, 90.0);
	// glLightfv(GL_LIGHT0, GL_POSITION, light_position1);

	// glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	// glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	// glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);

	// glEnable(GL_LIGHT0);
	glDisable(GL_LIGHT2);
	glDisable(GL_LIGHT1);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable( GL_BLEND );

	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
	glMateriali(GL_FRONT_AND_BACK, GL_SHININESS, shininess);

	textures = new GLuint[fileNames.size() + 1];
	glGenTextures(fileNames.size()+1, textures);

	for (std::vector<std::string>::size_type i = 0; i < fileNames.size(); ++i) {
		// std::cout << i << ", " << textures[i] << ", " << (glIsTexture(textures[i]) == GL_TRUE) <<": ";
		glBindTexture(GL_TEXTURE_2D, textures[i]);
		utilities::LoadTexture(fileNames[i].c_str(), textures[i]);
		// std::cout << i << ", " << textures[i] << ", " << (glIsTexture(textures[i]) == GL_TRUE) << std::endl;
	}
	glBindTexture(GL_TEXTURE_2D, textures[fileNames.size()]);
	utilities::LoadDummyTexture(textures[fileNames.size()]);

	currentDisplayList = glGenLists(nLists);
	std::cerr << "Base list ID: " << currentDisplayList << std::endl;

	pps_program = LoadShader("pp_vertex_shader.glsl", "pp_fragment_shader.glsl");
	texLocation = glGetUniformLocation(pps_program, "tex");
	glUseProgram(pps_program);
	glBindAttribLocation(pps_program, useTexIndex, "useTexture");
	glUniform1i(texLocation, 0);
	glActiveTexture(GL_TEXTURE0 + 0);
}

void display (void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	switch(currentCamera) {
		case 0:
			glLoadIdentity();
			gluLookAt(-.5 * utilities::myCos(b1->getPhi()) + b1->getX(), 0.8 + b1->getY() + 1.3, +.5 * utilities::mySin(b1->getPhi()) + b1->getZ(),
				-1.5 * utilities::myCos(b1->getPhi()) + b1->getX(), 0.6 + b1->getY() + 1.3, +1.5 * utilities::mySin(b1->getPhi()) + b1->getZ(),	0, 1, 0);
			break;
		case 1:
			glLoadIdentity();
			gluLookAt(6 * utilities::myCos(b1->getPhi()) + b1->getX(), 1 + b1->getY() + 1.3, -6 * utilities::mySin(b1->getPhi()) + b1->getZ(),
				b1->getX(), 0 + b1->getY() + 1.3, b1->getZ(),		0, 1, 0);
			break;
		case 2:
			glLoadIdentity();
			gluLookAt(roomSize, roomHeight-0.1, roomSize,
				0, roomSize/4, 0,
				0, 1, 0);
			break;
	}

	r1->draw();
	b1->draw();

	if (saveFramesToFile) {
		const size_t imgSize = 1280 * 720 * 3;
		unsigned char* data = new unsigned char[imgSize];
		glReadPixels(0,0, 1280,720, GL_RGB, GL_UNSIGNED_BYTE, data);
		// std::ofstream imgout(std::string("output/") + std::to_string(frameNum) + ".ppm", std::ios::out);
		std::ofstream imgout(std::string("output/") + std::to_string(frameNum) + ".ppm", std::ios::binary);
		// imgout << "P3\n1280 720\n255\n";
		imgout << "P6\n1280 720\n255\n";
		// for (int i = 0; i < 1280; ++i) {
		// 	for (int j = 0; j < 720; ++j) {
		// 		for (int k = 0; k < 3; ++k) {
		// 			imgout << (int)data[i*720*3 + j*3 + k] << " ";
		// 		}
		// 	}
		// 	imgout<<std::endl;
		// }
		imgout.write(reinterpret_cast<char*>(data), imgSize);
		imgout.close();
		delete [] data;
		frameNum++;
	}
	
	glFlush();
	glutSwapBuffers();
}

void computeVelocities() {
	int n = state.size();
	xVelocities.resize(n);
	zVelocities.resize(n);
	xVelocities[0] = velocity * -utilities::myCos(state[0][3]);
	xVelocities.back() = velocity * -utilities::myCos(state.back()[3]);
	zVelocities[0] = velocity * utilities::mySin(state[0][3]);
	zVelocities.back() = velocity * utilities::mySin(state.back()[3]);

	for (size_t i = 1; i < xVelocities.size()-1; ++i) {
		xVelocities[i] = 3 * ( (state[i+1][0] - state[i][0]) + (state[i][0] - state[i-1][0]) );
		zVelocities[i] = 3 * ( (state[i+1][2] - state[i][2]) + (state[i][2] - state[i-1][2]) );
	}

	{
		std::vector<double> a(n,1); a.back() = 0;
		std::vector<double> b(n,4); b[0] = 1; b.back() = 1;
		std::vector<double> c(n,1); c[0] = 0;
		utilities::solve(a,b,c,xVelocities,n);
	}
	{
		std::vector<double> a(n,1); a.back() = 0;
		std::vector<double> b(n,4); b[0] = 1; b.back() = 1;
		std::vector<double> c(n,1); c[0] = 0;
		utilities::solve(a,b,c,zVelocities,n);
	}
}

int compute() {
	if (currentSection >= state.size()-1) {
		std::cerr << "Animation done" <<std::endl;
		glutIdleFunc(NULL);
		return -1;
	}
	std::vector<double> &initState = state[currentSection];
	std::vector<double> &finalState = state[currentSection+1];
	currentCamera = cameraNum[currentSection];
	bool l1, l2;
	l1 = lightState[currentSection][0];
	l2 = lightState[currentSection][1];

	std::cerr << "Sanity check 1: ";
	for (int i = 0; i < 4; ++i) {
		std::cerr << initState[i] << " ";
	}
	std::cerr << std::endl;

	GLboolean b;
	glGetBooleanv(GL_LIGHT1, &b);
	if (l1 ^ (b == GL_TRUE)) toggleLight(utilities::HEADLIGHT);
	glGetBooleanv(GL_LIGHT2, &b);
	if (l2 ^ (b == GL_TRUE)) toggleLight(utilities::LAMP);
	
	std::cerr << "Sanity check 2: ";
	for (int i = 0; i < 4; ++i) {
		std::cerr << finalState[i] << " ";
	}
	std::cerr << std::endl;

	coeffs[0] = initState[0];
	coeffs[1] = xVelocities[currentSection];
	coeffs[4] = initState[2];
	coeffs[5] = zVelocities[currentSection];

	coeffs[2] = 3 * (finalState[0]-initState[0]) - 2 * xVelocities[currentSection] - 1 * xVelocities[currentSection+1];
	coeffs[3] = -2 * (finalState[0]-initState[0]) + xVelocities[currentSection] + xVelocities[currentSection+1];
	coeffs[6] = 3 * (finalState[2]-initState[2]) - 2 * zVelocities[currentSection] - 1 * zVelocities[currentSection+1];
	coeffs[7] = -2 * (finalState[2]-initState[2]) + zVelocities[currentSection] + zVelocities[currentSection+1];

	std::cerr << "Sanity check 3: ";
	for (int i = 0; i < 8; ++i) {
		std::cerr << coeffs[i] << " ";
	}
	std::cerr << std::endl;
	currentSection++;
	// glFlush();
	// glutPostRedisplay();
	return 0;
}

void processInput(std::ifstream& inFile) {
	std::vector<double> s[4];
	int c;
	std::vector<bool> l[2];
	std::string line;
	while(std::getline(inFile, line)) {
		std::stringstream ss;
		ss<<line;

		state.push_back( std::vector<double>() );
		lightState.push_back( std::vector<bool>() );

		for (int i = 0; i < 4; ++i){
			double d;
			ss>>d;
			state.back().push_back(d);
		}
		ss >> c;
		for (int i = 0; i < 2; ++i){
			bool b;
			ss >> b;
			lightState.back().push_back(b);
		}
		cameraNum.push_back(c);
	}

	std::cerr << "inputs processed: " <<std::endl;
	for (size_t i = 0; i < state.size(); ++i) {
		std::cerr << state[i][0] << " " << state[i][1] << " " << state[i][2] << " " << state[i][3] << " " << cameraNum[i] << " " << lightState[i][0] << " " << lightState[i][1] << std::endl;
	}
}

void timeStep(/*int rubbish = 1*/) {
	// int currentTime = glutGet(GLUT_ELAPSED_TIME);
	// float deltat = (currentTime - lastTime) * timeMultiplier;
	t += deltat;
	// lastTime = currentTime;
	if(deltat>=.5) std::cerr << "Warning, time step was " << deltat << std::endl;

	if(t >= 1.) {
		t -= 1.;
		int status = compute();
		if(status != 0) {
			std::cerr << "Animation done" <<std::endl;
			return;
		}
		// int currentTime = glutGet(GLUT_ELAPSED_TIME);
		// float deltat = (currentTime - lastTime) * timeMultiplier;
		t += deltat;
		// lastTime = currentTime;
		if(deltat>=.5) std::cerr << "Warning, time step was " << deltat << std::endl;
	}

	float t2 = t*t;
	float t3 = t*t2;

	float x = coeffs[3] * t3 + coeffs[2] * t2 + coeffs[1] * t + coeffs[0];
	float z = coeffs[7] * t3 + coeffs[6] * t2 + coeffs[5] * t + coeffs[4];

	float vx = 3 * coeffs[3] * t2 + 2 * coeffs[2] * t + coeffs[1];
	float vz = 3 * coeffs[7] * t2 + 2 * coeffs[6] * t + coeffs[5];

	float phi = std::atan2(vz, -vx) * 180. / M_PI;

	float ax = 6 * coeffs[3] * t + 2 * coeffs[2];
	float az = 6 * coeffs[7] * t + 2 * coeffs[6];

	float R = pow(vx*vx + vz*vz, 3./2.) / (ax*vz - az*vx);
	float thetaH = atan(BICYCLE::Length/R) * 180. / M_PI;

	// std::cerr << t << "\t" << x << "\t" << z << "\t" << phi << std::endl;

	float dx = x - b1->getX();
	float dz = z - b1->getZ();

	float dist = pow(dx*dx+dz*dz, 1./2.);
	float dRAngle = dist / BICYCLE::wheelSize * 180. / M_PI / (1 + BICYCLE::radius_ratio);
	float dFAngle = dRAngle / utilities::myCos(thetaH);
	b1->set(x, 0, z, phi, thetaH);
	b1->turn(dRAngle/BICYCLE::gearRatio, dRAngle, dFAngle);

	// glutTimerFunc(500, timeStep, 1);
	glFlush();
	glutPostRedisplay();
}

void reshape (int w, int h) {
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(30, (GLfloat) w/(GLfloat) h, 1.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glFlush();
	glutPostRedisplay();
}

/* Rotate about x-axis when "x" typed; rotate about y-axis
	when "y" typed; rotate about z-axis when "z" typed;
	"i" returns torus to original view */
void keyboard (unsigned char key, int x, int y) {
	switch (key) {
		case '1':
			currentCamera = 0;
			break;
		case '2':
			currentCamera = 1;
			break;
		case '3':
			currentCamera = 3;
			glLoadIdentity();
			gluLookAt(roomSize, roomHeight-0.1, roomSize,
				0, roomSize/4, 0,
				0, 1, 0);
			break;
		case 'h':
		case 'H':
			utilities::toggleLight(utilities::HEADLIGHT);
			break;
		case 'l':
		case 'L':
			utilities::toggleLight(utilities::LAMP);
			break;
		case 'r':
		case 'R':
			if (!(keyframeFile.is_open())) keyframeFile.open(keyframeFilename, std::ios_base::app);
			else std::cerr << "File already open." << std::endl;
			break;
		case 's':
		case 'S':
			if (keyframeFile.is_open()) {
				GLboolean l1, l2;
				glGetBooleanv(GL_LIGHT1, &l1);
				glGetBooleanv(GL_LIGHT2, &l2);
				keyframeFile << b1->getX() << "\t" << b1->getY() << "\t" << b1->getZ() << "\t" << b1->getPhi()
				<< "\t" << currentCamera << "\t" << (l1 == GL_TRUE) << "\t" << (l2 == GL_TRUE) << std::endl;
			}
			else std::cerr << "Record mode not active, ingnoring." << std::endl;
			break;
		case 'c':
		case 'C':
			if (keyframeFile.is_open()) {
				keyframeFile.close();
				keyframeFile.open(keyframeFilename, std::ofstream::out | std::ofstream::trunc);
				keyframeFile.close();
				keyframeFile.open(keyframeFilename, std::ios_base::app);
			}
			else std::cerr << "Record mode not active, ingnoring." << std::endl;
			break;
		case 'p':
		case 'P':{
			std::ifstream keyframeReadingFile(keyframeFilename, std::ios::in);
			std::string line;
			processInput(keyframeReadingFile);
			computeVelocities();
			if(compute() == 0) {
				lastTime = glutGet(GLUT_ELAPSED_TIME);
				glutIdleFunc(timeStep);
			}
			else std::cerr << "Not enough keyframes."<<std::endl;
			// glutTimerFunc(500, timeStep, 1);
			break;
		}
		case 'x':
		case 'X':
			saveFramesToFile = true ^ saveFramesToFile;
			break;
		case 27:
			exit(0);
			break;
	}
	glFlush();
	glutPostRedisplay();
}

void keyboardSpecial (int key, int x, int y) {
	switch (key) {
		case GLUT_KEY_LEFT:
			b1->left();
			break;
		case GLUT_KEY_RIGHT:
			b1->right();
			break;
		case GLUT_KEY_UP:
			b1->forward();
			break;
	}
	glFlush();
	glutPostRedisplay();
}

int main (int argc, char **argv) {
	glutInitWindowSize(1280, 720);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
	glutCreateWindow(argv[0]);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(keyboardSpecial);
	glutDisplayFunc(display);

	#ifndef __APPLE__
	glewInit();
	#endif
	init();

	#ifndef __APPLE__
	if (!(GLEW_ARB_vertex_shader && GLEW_ARB_fragment_shader)) {
		std::cout<<"Shaders (GLSL) not supported."<<std::endl;
		exit(1);
	}
	#endif

	r1 = new Room();
	b1 = new Bicycle;

	glutMainLoop();
	return 0;
}