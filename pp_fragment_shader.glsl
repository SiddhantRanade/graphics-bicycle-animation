#version 120
// https://www.opengl.org/sdk/docs/tutorials/ClockworkCoders/lighting.php

int lightIndexMin = 1;
int lightIndexMax = 2;

varying vec3 N;
varying vec3 v;

varying float vUseTex;

uniform sampler2D tex;

void main (void) {
	vec3 E = normalize(-v); // we are in Eye Coordinates, so EyePos is (0,0,0)
	vec4 Iamb = vec4(0.0,0.0,0.0,0.0);
	vec4 Idiff = vec4(0.0,0.0,0.0,0.0);
	vec4 Ispec = vec4(0.0,0.0,0.0,0.0);

	vec4 color;

	if (vUseTex < 500.) color = gl_FrontMaterial.diffuse;	// Use for both diffuse and ambient.  We are using glColorMaterial with GL_AMBIENT_AND_DIFFUSE, so this is correct
	else color = texture2D(tex, gl_TexCoord[0].xy);	// Gets texture color if texture bound, white otherwise.

	for (int lightIndex = lightIndexMin; lightIndex <= lightIndexMax; ++lightIndex){
		vec3 L = normalize(gl_LightSource[lightIndex].position.xyz - v);   
		vec3 R = normalize(-reflect(L,N));  
	
		//calculate Ambient Term:  
		Iamb += color * gl_LightSource[lightIndex].ambient; //gl_FrontLightProduct[lightIndex].ambient; //+ gl_BackLightProduct[lightIndex].ambient;    
	
		//calculate Diffuse Term:  
		Idiff += color * gl_LightSource[lightIndex].diffuse //gl_FrontLightProduct[lightIndex].diffuse 
					* max(dot(N,L), 0.0) 
					* ((-dot(L, gl_LightSource[lightIndex].spotDirection) > gl_LightSource[lightIndex].spotCosCutoff) ? 1 : 0);
			//+ gl_BackLightProduct[lightIndex].diffuse * max(-dot(N,L), 0.0) * ((dot(L, gl_LightSource[lightIndex].spotDirection) > gl_LightSource[lightIndex].spotCosCutoff) ? 1 : 0);
	
		// calculate Specular Term:
		Ispec += gl_FrontLightProduct[lightIndex].specular 
					* pow(max(dot(R,E),0.0),0.3*gl_FrontMaterial.shininess)
					* ((-dot(L, gl_LightSource[lightIndex].spotDirection) > gl_LightSource[lightIndex].spotCosCutoff) ? 1 : 0);
	}
	Idiff = clamp(Idiff, 0.0, 1.0);
	Ispec = clamp(Ispec, 0.0, 1.0);
	// write Total Color:  
	// gl_FragColor = gl_FrontLightModelProduct.sceneColor + Iamb + Idiff + Ispec;
	gl_FragColor = Iamb + Idiff + Ispec;
}